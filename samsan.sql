/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : sigma

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 04/12/2020 05:32:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for harga
-- ----------------------------
DROP TABLE IF EXISTS `harga`;
CREATE TABLE `harga`  (
  `id` int(11) NOT NULL,
  `jenispengiriman` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `berat` int(11) NULL DEFAULT NULL,
  `volume` int(11) NULL DEFAULT NULL,
  `harga` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of harga
-- ----------------------------
INSERT INTO `harga` VALUES (1, 'Standard', 1, 100, 10000);
INSERT INTO `harga` VALUES (2, 'Express', 1, 100, 15000);

-- ----------------------------
-- Table structure for tracking_log
-- ----------------------------
DROP TABLE IF EXISTS `tracking_log`;
CREATE TABLE `tracking_log`  (
  `idtransaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idtrackinglog` int(11) NOT NULL,
  `tracking_status` int(11) NULL DEFAULT NULL,
  `tanggal` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`idtransaksi`, `idtrackinglog`) USING BTREE,
  INDEX `fk1_trackinglog`(`tracking_status`) USING BTREE,
  CONSTRAINT `fk1_trackinglog` FOREIGN KEY (`tracking_status`) REFERENCES `tracking_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk2_trackinglog` FOREIGN KEY (`idtransaksi`) REFERENCES `transaksi` (`idtransaksi`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tracking_log
-- ----------------------------
INSERT INTO `tracking_log` VALUES ('TS0001', 1, 1, '2020-12-03 18:14:52');
INSERT INTO `tracking_log` VALUES ('TS0001', 2, 2, '2020-12-03 18:15:00');
INSERT INTO `tracking_log` VALUES ('TS0001', 4, 3, '2020-12-03 18:15:25');
INSERT INTO `tracking_log` VALUES ('TS0002', 3, 1, '2020-12-03 18:15:18');
INSERT INTO `tracking_log` VALUES ('TS0002', 5, 2, '2020-12-03 18:15:30');

-- ----------------------------
-- Table structure for tracking_status
-- ----------------------------
DROP TABLE IF EXISTS `tracking_status`;
CREATE TABLE `tracking_status`  (
  `id` int(11) NOT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tracking_status
-- ----------------------------
INSERT INTO `tracking_status` VALUES (1, 'OnProgress');
INSERT INTO `tracking_status` VALUES (2, 'OnDelivery');
INSERT INTO `tracking_status` VALUES (3, 'Delivered');

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi`  (
  `idtransaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `pengirim` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `penerima` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pengiriman` int(11) NULL DEFAULT NULL,
  `berat` int(11) NULL DEFAULT NULL,
  `volume` int(11) NULL DEFAULT NULL,
  `totalharga` int(11) NULL DEFAULT NULL,
  `isdelete` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idtransaksi`) USING BTREE,
  INDEX `fk1_transaksi`(`pengiriman`) USING BTREE,
  CONSTRAINT `fk1_transaksi` FOREIGN KEY (`pengiriman`) REFERENCES `harga` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES ('TS0001', '2020-12-03', 'Muhammad Riwandi', 'Shaldy Ramandani Zein', 'Kabupaten Bandung', 1, 3, 100, 30000, 0);
INSERT INTO `transaksi` VALUES ('TS0002', '2020-12-03', 'Sandi Rahayu', 'Denda Sandika', 'Banjaran Raya', 2, 1, 200, 30000, 0);

SET FOREIGN_KEY_CHECKS = 1;
