<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// 
// Route::get('/tes/{id}','TransaksiController@generateID');


// Route Dashboard
Route::get('/','TrackingController@index');

// Route Transaksi
Route::get('transaksi','TransaksiController@index');
Route::get('transaksi/create', 'TransaksiController@create');
Route::post('transaksi/create', 'TransaksiController@store');
Route::get('transaksi/destory/{id}','TransaksiController@destroy');
Route::post('transaksi/update/{id}','TransaksiController@update');
Route::post('print','TrackingController@print');