<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingStatus extends Model
{
    protected $table = 'tracking_status';
    protected $fillable = [
        'id',
        'status'
  ];
  public $timestamps = false;
}
