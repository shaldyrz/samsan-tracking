<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TrackingLog;
use App\Harga;
class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = [
        'idtransaksi',
        'tanggal',
        'pengirim',
        'penerima',
        'alamat',
        'pengiriman',
        'berat',
        'volume',
        'totalharga',
        'isdelete'
  ];
  public $timestamps = false;
  public function tracking_log(){
    return $this->hasMany(TrackingLog::class, 'idtransaksi', 'idtransaksi');
  }
  public function harga(){
    return $this->hasOne(Harga::class, 'idtransaksi', 'id');
  }
}
