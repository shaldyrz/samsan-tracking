<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\transaksi;
class Harga extends Model
{
    protected $table = 'harga';
    protected $fillable = [
        'id',
        'jenispengiriman',
        'berat',
        'volume',
        'harga'
  ];
  public $timestamps = false;
  public function transaksi(){
    return $this->hasOne(TrackingStatus::class, 'idtransaksi', 'id');
  }
}
