<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TrackingStatus;
class TrackingLog extends Model
{
    protected $table = 'tracking_log';
    protected $fillable = [
        'idtransaksi',
        'idtrackinglog',
        'tracking_status',
        'tanggal'
  ];
  public $timestamps = false;
  
  public function status(){
    return $this->hasOne(TrackingStatus::class, 'id', 'tracking_status');
  }
}
