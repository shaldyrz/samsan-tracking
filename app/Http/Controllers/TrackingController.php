<?php

namespace App\Http\Controllers;


use PDF;
use Illuminate\Support\Facades\DB;
use App\Transaksi;
use Illuminate\Http\Request;
class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['bulan'] = 
        [
            'Bulan',
            'Januari', 
            'Februari',
            'Maret',
            'April', 
            'Mei',
            'Juni',
            'Juli', 
            'Agustus',
            'September',
            'Oktober', 
            'November',
            'Desember',
        
        ];

        $data['tahun'] =
        [
            'Tahun',
            '2019',
            '2020',
            '2021'
        ];
        $data['data'] = Transaksi::with('tracking_log')->where('isdelete', '!=' , 1)->get();
        return view('admin.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // transaksi::get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function print(){
        $data['query'] = DB::select(DB::raw("SELECT
        idtransaksi,
        tanggal,
        pengirim,
        penerima,
        alamat,
        totalharga,
        (
        SELECT
            tracking_status.STATUS 
        FROM
            tracking_log
            INNER JOIN transaksi ON transaksi.idtransaksi = tracking_log.idtransaksi
            INNER JOIN tracking_status ON tracking_log.tracking_status = tracking_status.id 
        WHERE
            transaksi.idtransaksi = trx.idtransaksi 
        ORDER BY
            tracking_log.idtrackinglog DESC 
            LIMIT 1 
        ) AS status 
        FROM
            transaksi AS trx 
        WHERE
            isdelete = 0
            AND
                month(tanggal) = '".request()->bulan."'   
            AND
                year(tanggal) = '".request()->tahun."'   
            
                "));
        
        // dd($data);
        $bulans =
        [
            'Bulan',
            'Januari', 
            'Februari',
            'Maret',
            'April', 
            'Mei',
            'Juni',
            'Juli', 
            'Agustus',
            'September',
            'Oktober', 
            'November',
            'Desember',
        
        ];
        $data['bulan'] = $bulans[request()->bulan];
        $data['tahun'] = request()->tahun;
        return view('admin.report_pdf', $data);
    }

    public function getTransaksi($id){
        $transaksi = Transaksi::with(['tracking_log','tracking_log.status'])->where('idtransaksi',$id)->first();
        // dd($transaksi);
        if($transaksi){
            $data = [
                'status' => 'Data Found',
                'ID Transaksi' => $transaksi->idtransaksi,
                'tanggal' => $transaksi->tanggal,
                'Nama Pengirim' => $transaksi->pengirim,
                'Nama Penerima' => $transaksi->penerima,
                'Alamat Penerima' => $transaksi->alamat,
                'Tracking Status' => $transaksi->tracking_log()->orderBy('tanggal','desc')->first()->status->status,
            ];
        }else{
            $data['status'] = 'Data Not Found';
        }
        return response()->json($data);
    }
}
