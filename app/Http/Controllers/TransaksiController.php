<?php

namespace App\Http\Controllers;

use App\Harga;
use App\TrackingLog;
use App\TrackingStatus;
use Illuminate\Http\Request;
use App\Transaksi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['data'] = Transaksi::with('tracking_log')->where('isdelete', '!=' , 1)->get();
        $data['status'] = TrackingStatus::all();
        // dd($data);
        return view('transaksi.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    private function generateID(){
        $cekdb = Transaksi::orderBy('idtransaksi','desc')->first();
        // dd($cekdb);
        $idBaru = 1;
        if($cekdb){
            $lastTrxId = $cekdb->idtransaksi;
            $lastTrxId = str_replace('TS','',$lastTrxId);
            $lastTrxId = intval($lastTrxId);
            $idBaru = $lastTrxId+1;
            // dd($idBaru);
            
        }
        $trxIdBaru = 'TS'.str_pad($idBaru,4,'0',STR_PAD_LEFT);
        return $trxIdBaru;
    }
    
    private function generateIdTracking(){
        $idtrackinglog = TrackingLog::max('idtrackinglog') + 1;
        return $idtrackinglog;
    }

    public function create()
    {
        $jenisPengiriman = json_encode(Harga::get());
        $jenisPengirimans = Harga::get();
        return view('transaksi.form', compact('jenisPengiriman', 'jenisPengirimans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['idtransaksi'] = $this->generateID();
        $input['idtrackinglog'] = $this->generateIdTracking();
        $input['tanggal'] = Carbon::now()->setTimezone('Asia/Jakarta')->format("Y-m-d H:i:s");
        // dd($input);
        DB::beginTransaction();
        try {
            Transaksi::create([
                'idtransaksi' => $input['idtransaksi'],
                'tanggal' => $input['tanggal'], 
                'pengirim' => $input['pengirim'], 
                'penerima' => $input['penerima'], 
                'alamat' => $input['alamat'],
                'pengiriman' => $input['pengiriman'],
                'berat' => $input['berat'],
                'volume' => $input['volume'],
                'totalharga' => $input['totalharga'],
                'isdelete' => 0
            ]);
            TrackingLog::create([
                'idtransaksi' => $input['idtransaksi'],
                'idtrackinglog' => $input['idtrackinglog'],
                'tracking_status' => 1,
                'tanggal' => $input['tanggal']
            ]);
            DB::commit();
            return redirect('/transaksi')->with('success','Aksi Donasi berhasil dibuat'); 
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return redirect('/transaksi/create')->withinput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $var = $request->status;
        $data = TrackingLog::create([
            'idtransaksi' => $id,
            'tracking_status' => $var,
            'idtrackinglog' => $this->generateIdTracking(),
            'tanggal' => Carbon::now()->setTimezone('Asia/Jakarta')->format("Y-m-d H:i:s")
        ]);

        return response()->json(['Data berhasil di update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Transaksi::where('idtransaksi', $id)->update([
            'isdelete' => 1
        ]);
        return response()->json(['Data berhasil di hapus']);
    }
}
