@extends('layouts.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/backend/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Samsan</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu Aplikasi</a></li>
                            <li class="breadcrumb-item active">Tracking</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Tracking Table</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-sm-4">
                            <a href="{{url( '/transaksi/create') }}"
                                type="button" aria-pressed="true" class="btn btn-primary waves-effect waves-light btn-rounded mb-3 btn-withdraw">
                                    Tambah Data
                                    <i class=" mdi mdi-transfer-up">
                                    </i>
                            </a>
                        </div>
                        <table id="rdonasi-table" class="table w-100 nowrap">
                            <thead>
                                <tr style="text-align: center">
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Pengiriman</th>
                                    <th>Nama Pengirim</th>
                                    <th>Nama Penerima</th>
                                    <th>Alamat Penerima</th>
                                    <th>Total Harga</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr style="text-align: center">
                                    <td>{{ $item->idtransaksi }}</td>
                                    <td>{{ date('d M Y', strtotime($item->tanggal))}}</td>       
                                    <td>{{ $item->pengirim }}</td>
                                    <td>{{ $item->penerima }}</td>
                                    <td>{{ $item->alamat }}</td>
                                    <td>Rp{{ number_format(@$item->totalharga, 2, ',','.') }}</td>       
                                    @if ($item->tracking_log()->orderBy('tanggal','desc')->first()->tracking_status == 1)
                                        <td class="badge badge-warning" style="margin-top: 13px">
                                            OnProgres
                                        </td>
                                        @elseif ($item->tracking_log()->orderBy('tanggal','desc')->first()->tracking_status == 2)
                                            <td class="badge badge-primary" style="margin-top: 13px">
                                                OnDelivery
                                            </td>
                                        @elseif ($item->tracking_log()->orderBy('tanggal','desc')->first()->tracking_status == 3)
                                            <td class="badge badge-success" style="margin-top: 13px">
                                                Delivered
                                            </td>
                                        @endif
                                    <td>
                                        <a href="#">
                                            <button type="button" data-idtransaksi="{{ $item->idtransaksi }}" 
                                                class="btn btn-success btn-sm btn-track" data-placement="top" id="btn-track">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </a>
                                        <a href="#">
                                            <button type="button" data-idtransaksi="{{ $item->idtransaksi }}" 
                                                class="btn btn-danger btn-sm deleteConfirmation" data-placement="top" id="btn-delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </a>
                                    </td>       
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- container -->

    <div id="modal-file" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-file-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-file-title">Update Status Pengiriman</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="form-group mb-3">
                            <select class="form-control" name="updatestatus" data-toggle="select2" id="updatestatus">    
                                <option value="0">Update Status Pengiriman</option>
                                    @foreach ($status as $val)
                                        <option value="{{ $val->id }}" >{{ $val->status }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" id="btn-confirm" type="button">Update Data</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ asset('assets/js/chartjs.js') }}"></script>
        <script src="{{ URL::asset('assets/backend/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/flot-charts/flot-charts.min.js')}}"></script>

        <script>
            $(document).ready(function(){
                var urlUpdate = "";
                $(document).on('click','.btn-track', function () {
                    var idtransaksi = $(this).data('idtransaksi');
                    urlUpdate = "{{  url('transaksi/update') }}/"+idtransaksi;
                    $('#modal-file').modal('show');
                });

                $('#btn-confirm').on('click', function () {
                    $.ajax({
                        type:"post",
                        data: {
                                'status' : $('#updatestatus').val(),
                                '_token' : '{{ csrf_token() }}'
                                },
                        url: urlUpdate,
                        success: function (response) {
                            Swal.fire("Updated!", "Data ini telah diupdate.", "success");
                            location.reload();
                        }
                    });
                });
                $('.deleteConfirmation').click(function () {
                    var idtransaksi = $(this).data('idtransaksi');
                    Swal.fire({
                        title: "Apakah Anda yakin akan menghapus data ini?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Ya, Saya yakin"
                    }).then(function (result) {
                        if (result.value) {
                        $.ajax({
                            type: "get",
                            url: "{{ url('transaksi/destory')}}/" + idtransaksi,
                            success: function (response) {
                                Swal.fire("Deleted!", "Data ini telah dihapus.", "success");
                                location.reload();
                                // console.log(response)
                            }
                        });
                        }
                    });
                }); 
            });
        </script>
@endsection