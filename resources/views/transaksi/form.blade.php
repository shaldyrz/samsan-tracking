@extends('layouts.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/backend/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Samsan</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu Aplikasi</a></li>
                            <li class="breadcrumb-item active">Tambah data</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Tambah data baru</h4>

                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row mb-2">
            <div class="col-sm-4">
                <a href="{{ URL::previous() }}" role="button" aria-pressed="true" class="btn btn-dark waves-effect waves-light btn-rounded mb-3"><i class="mdi mdi-backburger"></i> Kembali</a>
            </div>
        </div> 
        <!-- end page title --> 
        <div class="card-box project-box">
            <form action="{{ url('/transaksi/create') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- <input type="hidden" name='idaksi' value="{{ $idaksi }}"> --}}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group mb-3">
                            <label for="pengirim">Nama Pengirim</label>
                            <input type="text" class="form-control" name="pengirim">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group mb-3">
                            <label for="penerima">Nama Penerima</label>
                            <input type="text" class="form-control" name="penerima">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group mb-3">
                            <label for="alamat">Alamat Penerima</label>
                            <input type="text" class="form-control" name="alamat">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group mb-3">
                            <label for="berat">Berat Barang</label>
                            <div class="input-group">
                                <input id="berat" type="number" class="form-control" name="berat" onkeyup="hitung()">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Kg</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group mb-3">
                            <label for="volume">Volume Barang</label>
                            <div class="input-group">
                                <input id="volume" type="number" class="form-control" name="volume" onkeyup="hitung()">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">volume</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group mb-3">
                            <label for="jenispengiriman">Jenis Pengiriman</label>
                            <select class="form-control" name="pengiriman" data-toggle="select2" id="jenisPengiriman" onchange="hitung()">
                                    <option value="0">Pilih jenis pengiriman</option>
                                    @foreach ($jenisPengirimans as $val)
                                        <option value="{{ $val->id }}" >{{ $val->jenispengiriman }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group mb-3">
                            <label for="namaaksi">Total Harga</label>
                            <input id="totalHarga" type="text" class="form-control" name="totalharga">
                        </div>
                    </div>
                    <div class="col-lg-12 text-right">
                        <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </div>
                </div>
            </form>
        </div> <!-- end card box-->

@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ asset('assets/js/chartjs.js') }}"></script>
        <script src="{{ URL::asset('assets/backend/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/flot-charts/flot-charts.min.js')}}"></script>
        
        <script>
            var jenisPengiriman = {!! $jenisPengiriman !!}
            var jenisPengirimanSel = document.getElementById("jenisPengiriman")
            var volume = document.getElementById("volume")
            var berat = document.getElementById("berat")
            var totalHarga = document.getElementById("totalHarga")

            function hitung(){
                //validasi data sebelum perhitungan
                if (jenisPengirimanSel.value == 0){
                    return 
                }

                if (volume.value == 0 || volume.value == null){
                    return 
                }

                if (berat.value == 0 || berat.value == null){
                    return
                }

                //ambil jenis pengiriman yang sesuai dengan id nya
                var _jenisPengiriman = null
                for (let val of jenisPengiriman){
                    if (val.id == jenisPengirimanSel.value){
                        _jenisPengiriman = val
                        break;
                    }
                }

                var harga = 0 //init
                var _volume = parseInt(volume.value) //ubah string ke int
                var _berat = parseInt(berat.value) 
                if (volume.value >= berat.value ){ //perbandingan
                    harga = _jenisPengiriman.harga * Math.ceil(_volume/100)
                }
                else {
                    harga = _jenisPengiriman.harga * _berat
                }

                totalHarga.value = harga //simpen harga ke tag input dengan id totalHarga
            }
        </script>
@endsection