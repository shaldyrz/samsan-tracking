<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                {{date('Y')}} &copy; by <a href="">Shaldy Ramandani Zein</a> 
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->