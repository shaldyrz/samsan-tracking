<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Menu Aplikasi</li>

                <li>
                    <a href="{{url( '/') }}">
                        <i class="fe-airplay"></i>
                        <span> Dashboards </span>
                    </a>
                </li>
                <li>
                    <a href="{{url( '/transaksi') }}">
                        <i class="fe-shopping-cart"></i>
                        <span>Transaksi </span>
                    </a>
                </li>
                {{-- <li>
                    <a href="javascript: void(0);">
                        <i class="fe-sidebar"></i>
                        <span> Update Transaksi </span>
                    </a>
                </li>
                 --}}
        </div>
        <!-- End Sidebar -->
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->