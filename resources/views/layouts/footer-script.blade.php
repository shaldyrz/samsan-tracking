        <!-- Vendor js -->
        <script src="{{ URL::asset('assets/backend/js/vendor.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/backend/js/app.min.js')}}"></script>
        
        @yield('script-bottom')