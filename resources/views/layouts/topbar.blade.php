<!-- Topbar Start -->
<div class="navbar-custom">
   <!-- LOGO -->
    <div class="logo-box">
        <a href="/" class="logo text-center">
            <span class="logo-lg">
                <img src="{{url('assets/logo2.jpg')}}" alt="" height="80px" width="100px">
                <!-- <span class="logo-lg-text-light">UBold</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-sm-text-dark">U</span> -->
                <img src="{{url('assets/logo2.jpg')}}" alt="" height="20">
            </span>
        </a>
    </div>
    
    <ul class="list-unstyled topnav-menu float-right mb-0">
        <li class="d-none d-sm-block">
            <form class="app-search">
                <div class="app-search-box">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                            <button class="btn" type="submit">
                                <i class="fe-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </li>
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                {{-- <img src="assets/images/users/user-1.jpg" alt="user-image" class="rounded-circle"> --}}
                <span class="pro-user-name ml-1">
                    Administrator <i class="mdi mdi-chevron-down"></i> 
                </span>
            </a>
        </li>
</div>
<!-- end Topbar -->