
        @yield('css')

        <!-- App css -->
        <link href="{{ URL::asset('assets/backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/backend/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/backend/css/app.min.css')}}" rel="stylesheet" type="text/css" />