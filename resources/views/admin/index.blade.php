@extends('layouts.master')

@section('css')
        <!-- Plugins css -->
        <link href="{{ URL::asset('assets/backend/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Samsan</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu Aplikasi</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        {{-- <h4 class="header-title">Daftar User</h4> --}}
                        <div class="col-sm-4">
                            <a href="#">
                                <button type="button" aria-pressed="true" class="btn btn-primary waves-effect waves-light btn-rounded mb-3 btn-report" id="btn-report">
                                        Print Data
                                        <i class=" mdi mdi-transfer-down"> </i>
                                </button>
                            </a>
                        </div>
                        <table id="rdonasi-table" class="table w-100 nowrap">
                            <thead>
                                <tr style="text-align: center">
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Pengiriman</th>
                                    <th>Nama Pengirim</th>
                                    <th>Nama Penerima</th>
                                    <th>Alamat Penerima</th>
                                    <th>Total Harga</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                    <tr style="text-align: center">
                                        <td>{{ $item->idtransaksi }}</td>
                                        <td>{{ date('d M Y', strtotime($item->tanggal))}}</td>
                                        <td>{{ $item->pengirim }}</td>
                                        <td>{{ $item->penerima }}</td>
                                        <td>{{ $item->alamat }}</td>
                                        <td>Rp{{ number_format(@$item->totalharga, 2, ',','.') }}</td>
                                        {{-- {{ $item->tracking_log[0]->tracking_status }} --}}
                                        @if ($item->tracking_log()->orderBy('tanggal','desc')->first()->tracking_status == 1)
                                        <td class="badge badge-warning" style="margin-top: 13px">
                                            OnProgres
                                        </td>
                                        @elseif ($item->tracking_log()->orderBy('tanggal','desc')->first()->tracking_status == 2)
                                            <td class="badge badge-primary" style="margin-top: 13px">
                                                OnDelivery
                                            </td>
                                        @elseif ($item->tracking_log()->orderBy('tanggal','desc')->first()->tracking_status == 3)
                                            <td class="badge badge-success" style="margin-top: 13px">
                                                Delivered
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->
    </div> <!-- container -->
    
    <div id="modal-report" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-report-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-report-title">Cetak Laporan Transaksi</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url("/print") }}" method="post" target="_blank">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-group mb-3">
                                    <select class="form-control" name="bulan" data-toggle="select2" id="bulan">    
                                            @foreach ($bulan as $key => $val)
                                                <option value="{{ $key }}" >{{ $val }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group mb-3">
                                    <select class="form-control" name="tahun" data-toggle="select2" id="tahun">    
                                            @foreach ($tahun as $key => $val)
                                                <option value="{{ $val }}" >{{ $val }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btn-confirm" type="submit">Print Data</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
        <!-- Plugins js-->
        <script src="{{ asset('assets/js/chartjs.js') }}"></script>
        <script src="{{ URL::asset('assets/backend/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/jquery-knob/jquery-knob.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/jquery-sparkline/jquery-sparkline.min.js')}}"></script>
        <script src="{{ URL::asset('assets/backend/libs/flot-charts/flot-charts.min.js')}}"></script>
        <script>
            $(document).ready(function () {
                $(document).on('click','.btn-report', function () {
                    var idtransaksi = $(this).data('idtransaksi');
                    // urlUpdate = "{{  url('transaksi/update') }}/"+idtransaksi;
                    $('#modal-report').modal('show');
                });
            });
        </script>
@endsection