<!DOCTYPE html>
<html lang="en">
<head>
    <!-- App css -->
    <link href="{{ URL::asset('assets/backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/backend/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/backend/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <title>Report Transaksi</title>
</head>
<body onload="window.print()">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{url('assets/logo2.jpg')}}" alt="" height="100px">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mt-3">
                        <h3 style="text-align: center">Laporan Transaksi Bulan {{ $bulan.' '.$tahun }}</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table mt-4 table-centered">
                                <tbody>
                                    <tr>
                                        <th>ID Transaksi</th>
                                        <th>Tanggal Pengiriman</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Alamat Penerima</th>
                                        <th>Total Harga</th>
                                        <th>Status</th>
                                    </tr>
                                    @foreach ($query as $item)
                                    <tr>
                                        <td>{{ $item->idtransaksi }}</td>
                                        <td>{{ date('d M Y', strtotime($item->tanggal))}}</td>
                                        <td>{{ $item->pengirim }}</td>
                                        <td>{{ $item->penerima }}</td>
                                        <td>{{ $item->alamat }}</td>
                                        <td>Rp{{ number_format(@$item->totalharga, 2, ',','.') }}</td>
                                        <td>{{ $item->status }}</td>
                                    </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>