---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_91efcca22fd11cc38bdd7e4ce0adf183 -->
## api/transaksi/{id}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/transaksi/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/transaksi/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "Data Not Found"
}
```

### HTTP Request
`GET api/transaksi/{id}`


<!-- END_91efcca22fd11cc38bdd7e4ce0adf183 -->

<!-- START_53be1e9e10a08458929a2e0ea70ddb86 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET /`


<!-- END_53be1e9e10a08458929a2e0ea70ddb86 -->

<!-- START_696c896e7a1076fea5587192315de237 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/transaksi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaksi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET transaksi`


<!-- END_696c896e7a1076fea5587192315de237 -->

<!-- START_26a678c6cfc362bad62aaf6971a8dbd9 -->
## transaksi/create
> Example request:

```bash
curl -X GET \
    -G "http://localhost/transaksi/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaksi/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET transaksi/create`


<!-- END_26a678c6cfc362bad62aaf6971a8dbd9 -->

<!-- START_f4386f76b7b8acf3fddc761312a43094 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/transaksi/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaksi/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST transaksi/create`


<!-- END_f4386f76b7b8acf3fddc761312a43094 -->

<!-- START_a635b972ed7fd1b7fe46dd3da05c37ee -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/transaksi/destory/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaksi/destory/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
[
    "Data berhasil di hapus"
]
```

### HTTP Request
`GET transaksi/destory/{id}`


<!-- END_a635b972ed7fd1b7fe46dd3da05c37ee -->

<!-- START_90496ded73e89df55a67338e3f37de08 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost/transaksi/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/transaksi/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST transaksi/update/{id}`


<!-- END_90496ded73e89df55a67338e3f37de08 -->

<!-- START_1aa4656eca29092aed75328701729c0f -->
## print
> Example request:

```bash
curl -X POST \
    "http://localhost/print" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/print"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST print`


<!-- END_1aa4656eca29092aed75328701729c0f -->


